﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your name");
            String name = Console.ReadLine();

            Console.WriteLine("Hello " + name);

            //Force the user to hit return, otherwise the app just exits without giving the 
            // user chance to see what's going on.
            Console.WriteLine("Press return to exit");
            Console.ReadLine();

        }
    }
}
