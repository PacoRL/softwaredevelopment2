﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AreaHouse
{
    class Program
    {
        class Room
        {
            public string name;
            public string length;
            public string width;
                public int getArea()
                {
                    int Area;
                    Area = Int32.Parse(this.length) * Int32.Parse(this.width);
                    return Area;
                }
        }
        static void Main(string[] args)
        {
            Boolean flag = false;
            Room[] House = new Room[10];
            int TotalRooms = 0;
            int AreaTotal = 0;
            

            while(flag == false)
            {
                House[TotalRooms] = new Room();
                Console.WriteLine("Please enter the room's description");
                House[TotalRooms].name = Console.ReadLine();
                Console.WriteLine("Please enter the room's length");
                House[TotalRooms].length = Console.ReadLine();
                Console.WriteLine("Please enter the room' s width");
                House[TotalRooms].width = Console.ReadLine();


                Console.WriteLine("The total area of this room are " + House[TotalRooms].getArea() + " meters");
                TotalRooms = TotalRooms + 1;

                Console.WriteLine("Have you finished adding rooms ? y/n ");

                if (Console.ReadLine() == "y")
                {
                    flag = true;
                }
            }

            for(int i = 0; i < TotalRooms; i++)
            {
                AreaTotal = AreaTotal + House[i].getArea();
            }
            Console.WriteLine("The House's total area is: " + AreaTotal.ToString());
            Console.ReadLine();


        }
    }
}
