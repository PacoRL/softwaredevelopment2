﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumbers
{
    class Program
    {
        class primeCalculator
        {
            public bool isPrime(int numb)
            {
                for(int i = 2; i < numb; i++)
                {
                    if(numb % i == 0)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        static void Main(string[] args)
        {
            int Number = 0;
            bool flag = false;
            primeCalculator Calc = new primeCalculator();

            while(flag == false)
            { 
                Console.WriteLine("Write a number between 1 and 10000");
                Number = Int32.Parse(Console.ReadLine());
                if (Number > 10000 || Number < 1)
                {
                    Console.WriteLine("Please, the number should be between 1 and 10000");
                }
                else
                {
                    flag = true;
                }
            }

            for (int i = Number; i > 1; i-- )
            {
                if (Calc.isPrime(i) == true)
                {
                    Console.WriteLine(i + " is a prime number");
                }
            }
            Console.ReadLine();
        }
    }
}
